import { expect } from 'chai';
import { UsersController } from '../lib/controllers/users.controller';
import {
  checkStatusCode,
  checkResponseTime,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';
const users = new UsersController();
const schemas = require('./data/schemas_allUsers.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Users controller`, () => {
  it(`should return 200 status code and all users when getting the user collection`, async () => {
    let response = await users.getAllUsers();
    checkStatusCode(response, 200);
  });

  it('should return that response time is less than 1000ms when getting then user registration', async () => {
    let response = await users.getAllUsers();
    checkResponseTime(response, 2000);
  });

  it('should return that response match to the all users response schema', async () => {
    let response = await users.getAllUsers();
    checkSchema(response, schemas.schemas_allUsers);
  });

  it('should return that response response body length have more than 1 item', async () => {
    let response = await users.getAllUsers();
    expect(
      response.body.length,
      'Response body length have more than 1 item'
    ).to.be.greaterThan(1);
  });
});

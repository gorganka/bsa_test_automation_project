import { expect } from 'chai';
import { PostsController } from '../lib/controllers/posts.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import { UsersController } from '../lib/controllers/users.controller';
import {
  checkStatusCode,
  checkResponseTime,
} from '../../helpers/functionsForChecking.helper';
const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();

const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Add comment to post`, async () => {
  let accessToken: string;
  let validCommentDataSet = [
    {
      authorId: global.appConfig.users.Tester['id'],
      postId: 100,
      body: 'first comment',
    },
    {
      authorId: global.appConfig.users.Tester['id'],
      postId: 105,
      body: 'second comment',
    },
    {
      authorId: global.appConfig.users.Tester['id'],
      postId: 102,
      body: 'third comment',
    },
  ];

  before(`User Login`, async () => {
    let login = await auth.login(
      global.appConfig.users.Tester['email'],
      global.appConfig.users.Tester['password']
    );

    accessToken = login.body.token.accessToken.token;
  });

  validCommentDataSet.forEach((credentials) => {
    it(`should create comments with valid credentials : '${credentials.authorId}' + '${credentials.postId}'+ '${credentials.body}'`, async () => {
      let postData: object = {
        authorId: credentials.authorId,
        postId: credentials.postId,
        body: credentials.body,
      };
      let response = await posts.addComment(postData, accessToken);

      expect(response.body.author.id).to.be.equal(postData['authorId']);
      expect(response.body.body).to.be.equal(postData['body']);

      checkStatusCode(response, 200);
      checkResponseTime(response, 2000);
    });
  });

  it(`should return 500  when user use invalid data`, async () => {
    let comData: object = {
      authorId: global.appConfig.users.Tester['id'],
      body: 'my comment is here',
    };
    let response = await posts.addComment(comData, accessToken);

    checkStatusCode(response, 500);
    checkResponseTime(response, 2000);
  });
});

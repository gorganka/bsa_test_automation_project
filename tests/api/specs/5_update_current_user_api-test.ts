import { expect } from 'chai';
import { UsersController } from '../lib/controllers/users.controller';
import {
  checkStatusCode,
  checkResponseTime,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';

const users = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_userdata.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Update User information`, async () => {
  let accessToken: string;
  before('Login and get the token', async () => {
    let response = await auth.login(
      global.appConfig.users.Gorganka['email'],
      global.appConfig.users.Gorganka['password']
    );
    accessToken = response.body.token.accessToken.token;
  });

  it('Updating User info', async () => {
    let userData: object = await users.getUserById(
      global.appConfig.users.Gorganka['id']
    );

    userData['body']['avatar'] = 'this is new avatar value';
    let response = await users.updateUser(userData['body'], accessToken);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });

  it(`that user info coresponded to changes`, async () => {
    let userData: object = await users.getUserById(
      global.appConfig.users.Gorganka['id']
    );
    expect(userData['body']['avatar']).to.be.equal('this is new avatar value');
  });

  it('should return that response is coresponded to the schema', async () => {
    let response = await users.getUserByToken(accessToken);
    checkSchema(response, schemas.schemas_userdata);
  });
});

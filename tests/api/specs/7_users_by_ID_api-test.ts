import { expect } from 'chai';
import { UsersController } from '../lib/controllers/users.controller';
import {
  checkStatusCode,
  checkResponseTime,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';
const users = new UsersController();
const schemas = require('./data/schemas_userdata.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Get User by ID`, () => {
  it(`should get access to user information by ID`, async () => {
    let response = await users.getUserById(
      global.appConfig.users.Gorganka['id']
    );

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkSchema(response, schemas.schemas_userdata);
  });

  let invalidCredentialsDataSet = [
    { id: 'hohoho' },
    { id: 0.76 },
    { id: '@@@' },
  ];

  invalidCredentialsDataSet.forEach((credentials) => {
    it(`should not find user using invalid credentials : '${credentials.id}'`, async () => {
      let response = await users.getUserById(credentials.id);

      checkStatusCode(response, 400 || 404);
      checkResponseTime(response, 3000);
    });
  });
});

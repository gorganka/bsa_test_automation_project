import { expect } from 'chai';
import { PostsController } from '../lib/controllers/posts.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import { UsersController } from '../lib/controllers/users.controller';
import {
  checkStatusCode,
  checkResponseTime,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';
const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();
const schemas = require('./data/schemas_add_post.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Add new post`, async () => {
  let accessToken: string;
  let postData: object = {
    authorId: global.appConfig.users.Tester['id'],
    previewImage: 'string',
    body: 'try to find this test post',
  };

  let validPostCredentialsDataSet = [
    {
      authorId: global.appConfig.users.Tester['id'],
      previewImage: 'string',
      body: 'first post',
    },
    {
      authorId: global.appConfig.users.Tester['id'],
      previewImage: 'string',
      body: 'second post',
    },
    {
      authorId: global.appConfig.users.Tester['id'],
      previewImage: 'string',
      body: 'third post',
    },
  ];

  before(`User Login`, async () => {
    let login = await auth.login(
      global.appConfig.users.Tester['email'],
      global.appConfig.users.Tester['password']
    );

    accessToken = login.body.token.accessToken.token;
  });

  validPostCredentialsDataSet.forEach((credentials) => {
    it(`should create post with valid credentials : '${credentials.authorId}' + '${credentials.previewImage}'+ '${credentials.body}'`, async () => {
      let postData: object = {
        authorId: credentials.authorId,
        previewImage: credentials.previewImage,
        body: credentials.body,
      };
      let response = await posts.createPost(postData, accessToken);
      checkStatusCode(response, 200);
      checkResponseTime(response, 1000);
      expect(response.body.author.id).to.be.equal(postData['authorId']);
      expect(response.body.body).to.be.equal(postData['body']);
      expect(response.body.previewImage).to.be.equal(postData['previewImage']);
      checkSchema(response, schemas.schemas_add_post);
    });
  });
});

import { expect } from 'chai';
import { PostsController } from '../lib/controllers/posts.controller';
import {
  checkStatusCode,
  checkResponseTime,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';

const users = new PostsController();
const schemas = require('./data/schemas_post.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Get all posts collection', () => {
  it(`should return 200 status code and all posts when getting the posts collection`, async () => {
    let response = await users.getAllPosts();
    checkStatusCode(response, 200);
    checkResponseTime(response, 2000);

    it(`should return that response body match to post collection schema`, async () => {
      let response = await users.getAllPosts();
      checkSchema(response, schemas.schemas_post);
    });

    it('should return that response response body length have more than 1 item', async () => {
      let response = await users.getAllPosts();
      expect(
        response.body.length,
        'Response body length have more than 1 item'
      ).to.be.greaterThan(1);
    });
  });
});

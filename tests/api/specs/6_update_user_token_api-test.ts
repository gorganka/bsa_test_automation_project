import { expect } from 'chai';
import { UsersController } from '../lib/controllers/users.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import {
  checkResponseTime,
  checkStatusCode,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';

const users = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_userdata.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Usage of token', () => {
  let accessToken: string;
  before('Login and get the token', async () => {
    let response = await auth.login(
      global.appConfig.users.Gorganka['email'],
      global.appConfig.users.Gorganka['password']
    );
    accessToken = response.body.token.accessToken.token;
  });

  it('should return that response is coresponded to updated user info', async () => {
    let response = await users.getUserByToken(accessToken);

    expect(response.body['avatar']).to.be.equal('this is new avatar value');
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
  });

  it('should return that response is coresponded to the schema', async () => {
    let response = await users.getUserByToken(accessToken);
    checkSchema(response, schemas.schemas_userdata);
  });
});

import { expect } from 'chai';
import { UsersController } from '../lib/controllers/users.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import { RegisterNewUserController } from '../lib/controllers/register.controller';
import {
  checkStatusCode,
  checkResponseTime,
} from '../../helpers/functionsForChecking.helper';

const auth = new AuthController();
const user = new UsersController();

const chai = require('chai');
chai.use(require('chai-json-schema'));

//I hide this test, because for current exist user it has one choice to pass the test
xdescribe(`Delete current user`, () => {
  let userId: number;
  let accessToken: string;

  before('Login user', async () => {
    let response = await auth.login(
      global.appConfig.users.Gorganka['email'],
      global.appConfig.users.Gorganka['password']
    );
    accessToken = response.body.token.accessToken.token;
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
  });

  it(`Delete User`, async () => {
    let response = await user.deleteUser(
      global.appConfig.users.Gorganka['id'],
      accessToken
    );
    checkStatusCode(response, 204);
  });

  it(`User is not exist checking`, async () => {
    let response = await user.getUserById(
      global.appConfig.users.Gorganka['id']
    );
    checkStatusCode(response, 404);
  });
});

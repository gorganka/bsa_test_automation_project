import { expect } from 'chai';
import { UsersController } from '../lib/controllers/users.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import {
  checkStatusCode,
  checkResponseTime,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';

const users = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_register.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Login user with valid credentials', () => {
  let accessToken: string;

  it(`should return all necessaries when login user is success `, async () => {
    let response = await auth.login(
      global.appConfig.users.Gorganka['email'],
      global.appConfig.users.Gorganka['password']
    );

    accessToken = response.body.token.accessToken.token;

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkSchema(response, schemas.schemas_register);

    expect(
      response.body.user.email,
      `Should correspond to user email`
    ).to.be.equal(global.appConfig.users.Gorganka['email']);
    expect(
      response.body.user.userName,
      `Should correspond to user userName`
    ).to.be.equal(global.appConfig.users.Gorganka['userName']);
  });
});

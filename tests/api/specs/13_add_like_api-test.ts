import { expect } from 'chai';
import { PostsController } from '../lib/controllers/posts.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import { UsersController } from '../lib/controllers/users.controller';
import {
  checkStatusCode,
  checkResponseTime,
} from '../../helpers/functionsForChecking.helper';

const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();

describe(`Add like reaction to post`, async () => {
  let accessToken: string;
  let likeData: object = {
    userId: global.appConfig.users.Tester['id'],
    entityId: 848,
    isLike: true,
  };

  before(`User Login`, async () => {
    let login = await auth.login(
      global.appConfig.users.Tester['email'],
      global.appConfig.users.Tester['password']
    );

    accessToken = login.body.token.accessToken.token;
  });

  it(`should return 200 OK when like is added`, async () => {
    let response = await posts.likePost(likeData, accessToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
  });
});

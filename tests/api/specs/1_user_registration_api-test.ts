import { expect } from 'chai';
import {
  checkResponseTime,
  checkStatusCode,
  checkSchema,
} from '../../helpers/functionsForChecking.helper';
import { RegisterNewUserController } from '../lib/controllers/register.controller';
//import { json } from 'node:stream/consumers';

const users = new RegisterNewUserController();
const schemas = require('./data/schemas_register.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

//this test just describe how we can create new user and test it. For other tests I use previously created unique user and save his credentials in the global variables
describe('New user registration', () => {
  let userData: object = {
    id: 0,
    avatar: 'avatar',
    email: 'not_a_current@gmail.com',
    userName: 'not_a_current',
    password: 'supersecurity',
  };
  let userID: number;
  it('should return 201 status code when getting then user registration', async () => {
    let response = await users.userRegistration(userData);
    userID = response.body.user['id'];
    checkStatusCode(response, 201);
  });
  it('should return that response time is less than 1000ms when getting then user registration', async () => {
    let response = await users.userRegistration(userData);
    checkResponseTime(response, 1000);
  });
  it('should return that response have the same user credentials as getting then user registration', async () => {
    let response = await users.userRegistration(userData);
    expect(response.body.user.email).to.be.equal(userData['email']);
    expect(response.body.user.userName).to.be.equal(userData['userName']);
    expect(response.body.user.avatar).to.be.equal(userData['avatar']);
  });
  it('should return that response is coresponded to the schema', async () => {
    let response = await users.userRegistration(userData);
    checkSchema(response, schemas.schemas_register);
  });
});

import { ApiRequest } from '../request';

const baseURL: string = global.appConfig.baseUrl;

export class UsersController {
  //Get All users list
  async getAllUsers() {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('GET')
      .url(`api/Users`)
      .send();
    return response;
  }

  //Get Users information by ID
  async getUserById(id) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('GET')
      .url(`api/Users/${id}`)
      .send();
    return response;
  }

  //Update user information
  async updateUser(userData: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('PUT')
      .url(`api/Users`)
      .body(userData)
      .bearerToken(accessToken)
      .send();
    return response;
  }

  async getUserByToken(accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('GET')
      .url(`api/Users/fromToken`)
      .bearerToken(accessToken)
      .send();
    return response;
  }

  async deleteUser(id: number, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('DELETE')
      .url(`api/Users/${id}`)
      .bearerToken(accessToken)
      .send();
    return response;
  }
}

import { ApiRequest } from '../request';

const baseURL: string = global.appConfig.baseUrl;

export class RegisterNewUserController {
  async userRegistration(userData: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('POST')
      .url(`api/Register`)
      .body(userData)
      .send();
    return response;
  }
}

import { ApiRequest } from '../request';

const baseURL: string = global.appConfig.baseUrl;

export class PostsController {
  //Get All users list
  async getAllPosts() {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('GET')
      .url(`api/Posts`)
      .send();
    return response;
  }

  async createPost(postData: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('POST')
      .url(`api/Posts`)
      .body(postData)
      .bearerToken(accessToken)
      .send();
    return response;
  }

  async addComment(comData: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('POST')
      .url(`api/Comments`)
      .body(comData)
      .bearerToken(accessToken)
      .send();
    return response;
  }

  async likePost(likeData: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('POST')
      .url(`api/Posts/like`)
      .body(likeData)
      .bearerToken(accessToken)
      .send();
    return response;
  }
}

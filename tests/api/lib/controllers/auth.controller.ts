import { ApiRequest } from '../request';

const baseURL: string = global.appConfig.baseUrl;

export class AuthController {
  async login(emailValue: string, passwordValue: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseURL)
      .method('POST')
      .url(`api/Auth/login`)
      .body({
        email: emailValue,
        password: passwordValue,
      })
      .send();
    return response;
  }
}

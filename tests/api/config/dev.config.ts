global.appConfig = {
  envName: 'DEV Environment',
  baseUrl: 'http://tasque.lol/',
  swaggerUrl: 'http://tasque.lol/index.html',

  users: {
    Gorganka: {
      id: 2829,
      email: 'my_user@gmail.com',
      userName: 'my_new_user',
      password: 'supersecurity',
    },
    Tester: {
      id: 1879,
      email: 'active_user@gmail.com',
      userName: 'active_user',
      password: '12345!',
    },
  },
};

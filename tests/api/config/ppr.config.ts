global.appConfig = {
  envName: 'PPR Environment',
  baseUrl: 'http://tasque.lol/',
  swaggerUrl: 'http://tasque.lol/index.html',

  users: {
    Gorganka: {
      id: 2758,
      email: 'one_new@gmail.com',
      userName: 'one_new_user',
      password: 'supersecurity',
    },
    Tester: {
      id: 1879,
      email: 'active_user@gmail.com',
      userName: 'active_user',
      password: '12345!',
    },
  },

  /*active_user: {
    Tester: {},
  },*/
};
